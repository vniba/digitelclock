const show: Element | any = document.querySelector('#show');
const locale: string = window.navigator.language;
const date = function () {
  return Date.now();
};

const call = () =>
  setInterval(() => {
    show.innerHTML = Intl.DateTimeFormat(locale, {
      timeStyle: 'medium',
    }).format(date());
  }, 1000);

window.addEventListener('load', () => {
  call();
});
